import React from "react";
import "./../styles/toolbar.scss";

const Toolbar = () => {
  return (
    <div className="toolbar">
      <button className="toolbar__btn brush"></button>
    </div>
  );
};

export default Toolbar;
